<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Role::class, 1)->create(['name' => 'ADMIN']);
    	factory(\App\Role::class, 1)->create(['name' => 'LESSOR']);
    	factory(\App\Role::class, 1)->create(['name' => 'LEESSEE']);
        factory(\App\Role::class, 1)->create(['name' => 'TEMPORAL']);

        factory(\App\User::class, 1)->create([
            'firstname' => 'Administrator',
            'role_id' => '1',
            'phone' => '4331008792',
            'email' => 'admin@rentala.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin$2020')
        ]);

        factory(\App\User::class, 1)->create([
            'firstname' => 'Arrendador',
            'role_id' => '2',
            'phone' => '4331008792',
            'email' => 'arrendador@rentala.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin$2020')
        ]);

        factory(\App\User::class, 1)->create([
            'firstname' => 'Arrendatario',
            'role_id' => '3',
            'phone' => '4331008792',
            'email' => 'arrendatario@rentala.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin$2020')
        ]);

        factory(\App\User::class, 1)->create([
            'firstname' => 'Temporal',
            'role_id' => '4',
            'phone' => '4331008792',
            'email' => 'temporal@rentala.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin$2020')
        ]);
    }
}
