<?php

use Illuminate\Database\Seeder;

class seeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(\App\Role::class, 1)->create(['name' => 'ADMIN']);
    	factory(\App\Role::class, 1)->create(['name' => 'LESSOR']);
    	factory(\App\Role::class, 1)->create(['name' => 'LEESSEE']);

        factory(\App\User::class, 1)->create([
            'firstname' => 'Administrator',
            'role_id' => '1',
            'phone' => '4331008792',
            'email' => 'admin@rentala.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin$2020')
        ]);
    }
}
