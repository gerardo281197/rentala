<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
            Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id')->references('id')->on('roles');

            $table->string('firstname');
            $table->string('phone');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->softDeletes();
            $table->timestamps();
        });


            /*Schema::create('lessors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');


            $table->softDeletes();
            $table->timestamps();
        });*/

            Schema::create('homes', function (Blueprint $table) {
            $table->bigIncrements('id');

            /*$table->unsignedBigInteger('lessor_id');
            $table->foreign('lessor_id')->references('id')->on('lessors');*/

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->text('description');
            $table->string('street');
            $table->string('neighborhood');
            $table->string('number')->nullable();
            $table->string('city');
            $table->string('price')->nullable();
            $table->string('ine');
            $table->string('proof_of_address');
            $table->string('front_hause');
            $table->string('inside1')->nullable();
            $table->string('inside2')->nullable();
            $table->string('inside3')->nullable();
            $table->boolean('state')->default(0);
            $table->boolean('state_rent')->default(1);


            $table->softDeletes();
            $table->timestamps();
        });

            Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            //$table->unsignedBigInteger('home_id');
            //$table->foreign('home_id')->references('id')->on('homes');

            $table->string('comment')->nullable()->default("Buen trabajo!");

            //$table->string('calification');
            $table->float('score')->default(10);



            $table->softDeletes();
            $table->timestamps();
        });

            Schema::create('i_want_to_rent', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('home_id');
            $table->foreign('home_id')->references('id')->on('homes');

            $table->string('day_1');
            $table->string('day_2');
            $table->string('hour');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('lessors');
        Schema::dropIfExists('homes');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('i_want_to_rent');
    }
}
