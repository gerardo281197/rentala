-<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
	$name = $faker->name;
    $role = $faker->randomElement([3,2]);
    return [
        'role_id' => $role,
        'firstname' => $faker->name,
        'firstname' => $faker->randomElement([1, 0]),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '1234567890', // password
        
    ];
});
