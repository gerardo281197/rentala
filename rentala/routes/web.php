<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//------------------ welcome----------------
Route::get('/', function () {
    return view('welcome');
});

//-----------Login and register------------------------
Auth::routes();
Route::get('/admin/logout', function(){
    Session::flush();
    Auth::logout();
    return Redirect::to("/");
})->name('logout2');
//--------- home ----------------------------------------
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/comments', 'Controller@comments')->name('comments');


Route::group(['middleware' => 'auth'], function () {
	Route::group(['middleware' => 'admin'], function () {
		//-----------  admin -------------------------------------
		Route::get('/home-admin','AdminController@index')->name('home-admin');
		Route::get('/users-admin','AdminController@users')->name('users-admin');
	});
	Route::group(['middleware' => 'lessor'], function () {
		//-----------  lessor -------------------------------------
		Route::get('/register-home','HomeController@show')->name('register-home');
		Route::get('/show-homes','HomeController@showhomes')->name('show-homes');
		Route::post('/registerhause','HomeController@registerhause')->name('registerhause');
	});
	Route::group(['middleware' => 'leessee'], function () {
		//-----------  leessee -------------------------------------
		Route::get('/houses','LesseeController@index')->name('home-lessee');
		Route::get('/info/home/{hid}','Controller@moreinformation')->name('moreinformation');
	});
});


