<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//----------  Casas ---------------------------------------
Route::get('homes/{uid}','API\HomeController@show');
Route::post('home/online/{id}','API\HomeController@homeonline');
Route::post('home/offline/{id}','API\HomeController@homeoffline');
Route::delete('homes/{id}','API\HomeController@delete');
Route::post('edit/home/{hid}','API\HomeController@edithome');


//------------------- admin -------------------
Route::get('homes/admin/{uid}','API\AdminController@index');
Route::post('home/aprobbed/{hid}','API\AdminController@aprobbedhome');
Route::post('home/desaprobbed/{hid}','API\AdminController@desaprobbedhome');
Route::post('admin/edit/user/{uid}','API\AdminController@adminedituser');
Route::get('users','API\AdminController@users');
Route::get('restore/user/{uid}','API\AdminController@restoreuser');
Route::delete('delete/user/{uid}','API\AdminController@deleteuser');

// --------------- comments ---------------------------
Route::post('coment/{uid}','Controller@coment');