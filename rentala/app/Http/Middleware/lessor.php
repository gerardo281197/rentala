<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Role;

class lessor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        /*dd("hola desde Middleware");
        return $next($request);*/
        $role = Auth::User()->role_id;

        if ($role == Role::LESSOR || $role == Role::ADMIN) {
            return $next($request); 
        }
        else {
            abort(403);
        }
    }
}
