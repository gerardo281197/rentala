<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Role;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*dd("hola desde Middleware");
        return $next($request);*/
        $role = Auth::User()->role_id;

        if ($role == Role::ADMIN) {
            return $next($request); 
        }
        else {
            abort(403);
        }
    }
}
