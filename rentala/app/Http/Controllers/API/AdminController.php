<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Homes;
use App\Comment;
use App\User;
use Auth;

class AdminController extends Controller
{
   public function index(Request $request, $uid){
   	$homes=Homes::all();
    $coments=Comment::all();
    $users=User::all();
   	return response()->json(array('status' => true, 'homes' => $homes,'coments' => $coments,'users' => $users));

    //return($homes);
   }

   public function aprobbedhome(Request $request, $hid){
	   	DB::beginTransaction();
        try {
            $home=Homes::find($hid);
            $home->state=1;
            $home->update();
            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Ha cambiado el estado a Aprobado"), 'home' => $home));
            //return($home);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
   }

   public function desaprobbedhome(Request $request, $hid){
	   	DB::beginTransaction();
        try {
            $home=Homes::find($hid);
            $home->state=0;
            $home->update();
            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Ha cambiado el estado a Desaprobado"), 'home' => $home));
            //return($home);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
   }
   public function users(Request $request){
    $users=User::withTrashed()->get();
    return response()->json(array('status' => true, 'users' => $users));
   }

   public function deleteuser(Request $request, $uid){
        try {
            $coments=Comment::where('user_id', '=', $uid)->delete();
            $home=Homes::where('user_id', '=', $uid)->delete();
            $user=User::find($uid);
            $user->delete();         
            return response()->json(array('status' => true, 'message' => __("Ha eliminado el usuario")));
            //return($coments);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
   }
   public function restoreuser(Request $request, $uid){
        try {
            $coment_user=Comment::onlyTrashed()->where([['deleted_at', '!=', null],['user_id',$uid]])->restore();
            $home_user=Homes::onlyTrashed()->where([['deleted_at', '!=', null],['user_id',$uid]])->restore();
            $user=User::onlyTrashed()->where('id',$uid)->restore();
     
            return response()->json(array('status' => true, 'message' => __("Usuario recuperado")));
            //return($user);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
   }
   public function adminedituser(Request $request, $uid){
    DB::beginTransaction();
        try {
            $user=User::find($uid);
            $em=$request->email;
            $pas=$request->password;
            if ($em != null) {
              $user->email=$request->email;
            }
            if ($pas != null) {
              $pass=$request->password;
              $new_pass=bcrypt($request->password);
              $user->password=$new_pass;
            }
            
            $user->update();
            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Ha cambiado datos del usuario")));
            //return($request->email);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
   }
}
