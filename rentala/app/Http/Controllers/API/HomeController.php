<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Homes;
use App\User;
use App\Comment;
use Auth;

class HomeController extends Controller
{
   public function show(Request $request, $uid){
   	$homes=Homes::where('user_id', '=', $uid)->get();
    $coments=Comment::where('id', '>', 0)->orderBy('created_at','desc')->get();
    $users=User::all();
   	return response()->json(array('status' => true, 'homes' => $homes,'coments' => $coments,'users' => $users));
    //return($homes);
   }

   public function delete(Request $request, $id){
   	try {
   		$home = Homes::findOrFail($id);
        $home->delete();
        return response()->json(array('status' => true, 'message' => __("Casa eliminada con exito"), 'id' => intval($id)));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
        //return($id);
    }

    public function homeonline(Request $request, $id){
	   	DB::beginTransaction();
        try {
            $home=Homes::find($id);
            $home->state_rent=1;
            $home->update();
            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Ha cambiado el estado a Disponible"), 'home' => $home));
            //return($home);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
	   	//return response()->json(array('status' => true, 'homes' => $homes));
   }
    public function homeoffline(Request $request, $id){
	   	DB::beginTransaction();
        try {
            $home=Homes::find($id);
            $home->state_rent=0;
            $home->update();
            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Ha cambiado el estado a En renta"), 'home' => $home));
            //return($home);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
	   	//return response()->json(array('status' => true, 'homes' => $homes));
   }

   public function edithome(Request $request, $hid){
      DB::beginTransaction();
        try {
            $home=Homes::find($hid);
            if ($request->price != null) {
              $home->price= $request->price;
            }
            if ($request->description != null) {
              $home->description= $request->description;
            }
            $home->update();
            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Actualización con exito")));
            //return($home);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
   }
}
