<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Comment;
use App\Homes;
use App\User;
use Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     public function comments(Request $request){
        return view('comments.comments');
    }

    public function coment(Request $request, $uid){
        DB::beginTransaction();
        try {
            $coment = new Comment();
            $coment->user_id = $uid;
            if ($request->get('comment') != null) {
            	$coment->comment = $request->get('comment');
            }

            if ( $request->get('score') != null) {
            	$coment->score = $request->get('score');
            }


            $coment->save();

            DB::commit();
            return response()->json(array('status' => true, 'message' => __("Gracias por tú comentario")));
        } catch (Exception $e) {
            DB::rollBack();
            return($e->getMessage());
        }
    }

    public function moreinformation(Request $request, $hid){
        $home_user=Homes::where('id','=',$hid)->pluck('user_id');
        $home=Homes::where('id','=',$hid)->get();
        $user=User::where('id','=',$home_user)->get();
        //return($home);
        return view('lessee.moreinformation')->with('homes', $home)->with('users', $user);
    }

}
