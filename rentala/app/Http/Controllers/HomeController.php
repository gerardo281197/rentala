<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Homes;
use App\Comment;
use Storage;
use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderablevfdslajkbalksjdflakjsdfnjlkasdfhlkasjdfhlksajdfhlkashdflkash
     */
    public function index()
    {   
        $uid=Auth::user()->role_id;
        if ($uid == 1) {
            return redirect('home-admin');
        }
        elseif ($uid == 2) {
            return view('homes.show');           
        }
        elseif ($uid == 3) {
           return redirect('/houses');
        }
        //return($uid);
    }

    public function show(){
        return view('homes.register-home');
    }

    public function showhomes(){
        return view('homes.show');
    }

    public function registerhause(Request $request){
        $uid=Auth::user()->id;

        $ine=$request->file('ine');
        $paddress=$request->file('p-address');
        $fronthause=$request->file('front-hause');
        $inside1=$request->file('inside-1');
        $inside2=$request->file('inside-2');
        $inside3=$request->file('inside-3');
        DB::beginTransaction();
        try {
            $home = new Homes();
            $home->user_id = $uid;
            $home->description = $request->get('description');
            $home->street = $request->get('street');
            $home->neighborhood = $request->get('neighborhood');
            $home->number = $request->get('number');
            $home->price = $request->get('price');
            $home->city = $request->get('city');

            if ($ine != null){
                $home->ine=$ine->getClientOriginalName();
            }

            if ($paddress != null){
                $home->proof_of_address=$paddress->getClientOriginalName();
            }
  
            if ($fronthause != null){
                $home->front_hause=$fronthause->getClientOriginalName();
            }

            if ($inside1 != null){
                $home->inside1=$inside1->getClientOriginalName();
            }

            if ($inside2 != null){
                $home->inside2=$inside2->getClientOriginalName();
            }

            if ($inside3 != null){
                $home->inside3=$inside3->getClientOriginalName();
            }

            $home->save();

            DB::commit();
            if ($ine != null){
                $name_lessor=Auth::user()->email;
                $inee = $request->file('ine');
                $number=$request->get('number');
                Storage::disk('homes')->putFileAs($name_lessor, $inee, 'ine' . '_' . $number.'.jpeg');
            }

            if ($paddress != null){
                $name_lessor=Auth::user()->email;
                $paddresss = $request->file('p-address');
                Storage::disk('homes')->putFileAs($name_lessor, $paddresss, 'proof-address'. '_' . $number.'.jpeg');
            }

            if ($fronthause != null){
                $name_lessor=Auth::user()->email;
                $fronthausee = $request->file('front-hause');
                Storage::disk('homes')->putFileAs($name_lessor, $fronthausee, 'front-hause'. '_' . $number.'.jpeg');
            }

            if ($inside1 != null){
                $name_lessor=Auth::user()->email;
                $inside11 = $request->file('inside-1');
                Storage::disk('homes')->putFileAs($name_lessor, $inside11, 'inside-1'. '_' . $number.'.jpeg');
            }

            if ($inside2 != null){
                $name_lessor=Auth::user()->email;
                $inside22 = $request->file('inside-2');
                Storage::disk('homes')->putFileAs($name_lessor, $inside22, 'inside-2'. '_' . $number.'.jpeg');
            }

            if ($inside3 != null){
                $name_lessor=Auth::user()->email;
                $inside33 = $request->file('inside-3');
                Storage::disk('homes')->putFileAs($name_lessor, $inside33, 'inside-3'. '_' . $number.'.jpeg');
            }

            $request->session()->flash('alert-success','Home Saved Succesfull');
            return redirect()->route('show-homes');

        } catch (Exception $e) {
            DB::rollBack();
            return($e->getMessage());
        }

        //dd($lessorid);*/
    }

    public function comments(Request $request){
        return view('comments.comments');
    }
}
