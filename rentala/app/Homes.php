<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Homes extends Model
{
    use SoftDeletes;
    protected $table = 'homes';

    public function user(){
        return $this->hasToMany(User::class);
    }
}
