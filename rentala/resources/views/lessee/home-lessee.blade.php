@extends('layouts.app')
@extends('scripts.complements')
  <!---@section('form')
      Login Form -->
@section('content')

    <!---<div class="container col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>houses available</h2>
            </div>
            @if ($homes->isEmpty())
                <div>Sorry no houses available :(</div>
            @else
                    <tbody>
                        @foreach($homes as $home)
            -show house in card 
                        <div class="card">
        <div class="card-header">House Number {!! $home->number !!}</div>
        <div class="card-body">

             <p class="card-text">
             <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                            @foreach($users as $user)
                                @php($us_id = $user->id)
                            @endforeach
                            @if($us_id =$home->user_id)
                             @php ($get_email=$home->user_id)
                                @foreach($users->where('id', $get_email) as $emailuser)
                                    <img class="d-block w-100" width="5" height="250" src="storage/homes/{{$emailuser->email}}/{{'inside-1'. '_' . $home->number}}.jpeg" alt="algo">
                                @endforeach
                            @endif
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="images/{{$home->front_hause}} " alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/{{$home->inside1}} " alt="Second slide">
                        </div>
                        <div class="carousel-item">
                         <img class="d-block w-100" src="images/{{$home->inside2}} " alt="Third slide">
                        </div>
                        <div class="carousel-item">
                         <img class="d-block w-100" src="images/{{$home->inside3}} " alt="Third slide">
                        </div>
                    </div>
            </div></p>
            <h5 class="card-title">
                Street: {!! $home->street !!}<br>
                Description: {!! $home->description !!}<br>
                City: {!! $home->city !!}<br>
                </h5>
            <a href="#" class="btn btn-primary">I want this house</a>
        </div>
        <div class="card-footer">Price: $ {!! $home->price !!}</div>
    </div>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>--->
    @if ($homes->isEmpty())
        <div>Sorry no houses available :(</div>
    @else
        @foreach($homes as $home)
            @foreach($users as $user)
                @php($us_id = $user->id)
            @endforeach
            @if($us_id =$home->user_id)
                @php ($get_email=$home->user_id)
            @endif
            <hr>
            <div class="row row-cols-1 row-cols-md-8">
              <div class="col mb-8">
                <div class="card">
                    @foreach($users->where('id', $get_email) as $emailuser)
                        <img class=""  src="storage/homes/{{$emailuser->email}}/{{'front-hause'. '_' . $home->number}}.jpeg" alt="algo">
                    @endforeach
                  <!---<img src="..." class="card-img-top" alt="...">-->
                  <div class="card-body">
                    <h5 class="card-title">Calle: {!! $home->street !!} #{!! $home->number !!}</h5>
                    <h5 class="card-title">Colonia: {!! $home->neighborhood !!}</h5>
                    <hr>
                    <h5 class="card-title">Descripción: {!! $home->description !!}</h5>
                    <hr>
                    <h5 class="card-title">${!! $home->price !!} al mes</h5>
                    <hr>
                    <a type="button" class="btn btn-primary btn-lg btn-block" href="/info/home/{{$home->id}}">Más información</a>
                  </div>
                </div>
              </div>
            </div>
        @endforeach
    @endif
@endsection