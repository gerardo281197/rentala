@extends('layouts.app')
<!---@extends('scripts.complements')-->

@section('content')
<div class="container">
    <div class="flas_message">
        @foreach(['danger','warning','success','info'] as $msg)
            @if(Session::has('alert-'.$msg))
                <p class="alert alert-{{$msg}}">
                    {{ Session::get('alert-'.$msg) }}
                    <a href="#" class="close" data-dismiss="alert" arial-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>
    <div class="row justify-content-center">
        <div class="col-md-15">
            <div class="card">
                <div class="card-header">{{ __('Welcome') }}
                </div>

                <div class="card-body">


                    <all-comments-component :uid=4></all-comments-component>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
