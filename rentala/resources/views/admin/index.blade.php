@extends('layouts.app')
@extends('scripts.complements')

@section('content')
<div class="container">
    <div class="flas_message">
        @foreach(['danger','warning','success','info'] as $msg)
            @if(Session::has('alert-'.$msg))
                <p class="alert alert-{{$msg}}">
                    {{ Session::get('alert-'.$msg) }}
                    <a href="#" class="close" data-dismiss="alert" arial-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>
    <div class="row justify-content-center">
        <div class="col-md-15">
            <div class="card">
                <div class="card-header">{{ __('Welcome') }} {{Auth::user()->firstname}}
                	<a type="button" class="btn btn-primary float-right" href="{{ route('register-home') }}">Registra una casa</a>
                </div>

                <div class="card-body">


                    <index-component-admin :uid={{Auth::user()->id}}></index-component-admin>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
