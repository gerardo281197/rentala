<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registra tu Primer casa</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-login-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Registra tu Primer casa y empieza a ganar dinero!</h1>
                  </div>
                  <form method="POST" action="{{ route('registerhause') }}" enctype="multipart/form-data">
                    @csrf
             
                     <div class="form-group row">
                           <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descrive your hause') }}</label>
             
                                 <div class="col-md-6">
                                     <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="name" autofocus></textarea>
             
                                     @error('description')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="street" type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" required autocomplete="street" autofocus>
             
                                     @error('street')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="neighborhood" class="col-md-4 col-form-label text-md-right">{{ __('Neighborhood') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="neighborhood" type="text" class="form-control @error('neighborhood') is-invalid @enderror" name="neighborhood" value="{{ old('neighborhood') }}" required autocomplete="neighborhood" autofocus>
             
                                     @error('neighborhood')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Number of Hause') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="number" type="number" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ old('number') }}" required autocomplete="number" autofocus>
             
                                     @error('number')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required autocomplete="city" autofocus>
             
                                     @error('city')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>

                             <div class="form-group row">
                                 <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price of Hause') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="price" type="number" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" required autocomplete="price" autofocus>
             
                                     @error('price')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="ine" class="col-md-4 col-form-label text-md-right">{{ __('INE') }}</label>
             
                                 <div class="col-md-6">
                                     <input  accept="image/jpeg,jpg" type="file" class="form-control @error('ine') is-invalid @enderror" name="ine" v required autocomplete="ine" autofocus>
             
                                     @error('ine')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="p-address" class="col-md-4 col-form-label text-md-right">{{ __('Proof Address') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="p-address" type="file" accept="image/jpeg,jpg" class="form-control @error('p-address') is-invalid @enderror" name="p-address" value="{{ old('p-address') }}" required autocomplete="p-address" autofocus>
             
                                     @error('p-address')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="ine" class="col-md-4 col-form-label text-md-right">{{ __('Front Hause') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="front-hause" type="file" accept="image/jpeg,jpg" class="form-control @error('front-hause') is-invalid @enderror" name="front-hause" value="{{ old('front-hause') }}" required autocomplete="front-hause" autofocus>
             
                                     @error('front-hause')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="inside-1" class="col-md-4 col-form-label text-md-right">{{ __('Inside 1') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="inside-1" type="file" accept="image/jpeg,jpg" class="form-control @error('inside-1') is-invalid @enderror" name="inside-1" value="{{ old('front-hause') }}" required autocomplete="front-hause" autofocus>
             
                                     @error('inside-1')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="inside-2" class="col-md-4 col-form-label text-md-right">{{ __('Inside 2') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="inside-2" type="file" accept="image/jpeg,jpg" class="form-control @error('inside-2') is-invalid @enderror" name="inside-2" value="{{ old('front-hause') }}" required autocomplete="front-hause" autofocus>
             
                                     @error('inside-2')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row">
                                 <label for="inside-3" class="col-md-4 col-form-label text-md-right">{{ __('Inside 3') }}</label>
             
                                 <div class="col-md-6">
                                     <input id="inside-3" type="file" accept="image/jpeg,jpg" class="form-control @error('inside-3') is-invalid @enderror" name="inside-3" value="{{ old('front-hause') }}" required autocomplete="front-hause" autofocus>
             
                                     @error('inside-3')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                 </div>
                             </div>
             
                             <div class="form-group row mb-0">
                                 <div class="col-md-6 offset-md-4">
                                     <button type="submit" class="btn btn-primary">
                                         {{ __('Save Hause') }}
                                     </button>
                                 </div>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
             </div>
             </div>
             
                  <hr>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>

