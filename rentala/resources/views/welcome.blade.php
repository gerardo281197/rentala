@extends('layouts.layout')
@section('nvar')
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Rentala</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('comments') }}">Comentarios</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Conviertete en Anfitrion</a>
          </li>
         <!--  <li class="nav-item">
            <a class="nav-link" href="#">Sobre Nosotros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Servicios</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contacto</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/login') }}">Iniciar Sesión</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
@endsection
@section('content')
  <!-- Header -->
  <header class="header">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-lg-12">
          <h1 class="display-4 text-white mt-5 mb-2">Rentala la plataforma donde encontraras la casa ideal para ti y tu familia</h1>
          <p class="lead mb-5 text-white-50">¿Estas por mudarte o buscas hospedarte por algunos dias? Encuentra el mejor hogar para tu familia, encontraras casas, departamentos y mas, estas a un solo click</p>
        </div>
      </div>
    </div>
  </header>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <div class="col-md-8 mb-5">
        <h2>¿Tienes una casa, local o departamento  para rentar?</h2>
        <hr>
        <p>Publica tu inmueble con nostros y llega a mas clientes potenciales</p>
        <p>Te ofrecemos:
        <br>-Plataforma que impulsa tus ganacias al llegar a millones de clientes por todo el mundo
        <br>-olvidate de ir a cobrar la renta, deja que tus huespedes la paguen directo a tu tarjeta bancaria desde nuestra Plataforma
        </p>
        <a href="#" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Quiero mas Informacion</a>
      </div>
      <div class="col-md-4 mb-5">
        <h2>Contacto</h2>
        <hr>
        <address>
          <strong>Sombrerete Zac.</strong>
          <br>Julio Tapia 35
          <br>Colonial el Coronel, Zac 99100
          <br>
        </address>
        <address>
          <abbr title="Phone">Telefono:</abbr>
          (433) 105 0000
          <br>
          <abbr title="Email">Email:</abbr>
          <a href="mailto:#">soporte@rentala.com.mx</a>
        </address>
      </div>
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <img class="card-img-top" src="images/house.svg" alt="">
          <div class="card-body">
            <h4 class="card-title">Casas</h4>
            <p class="card-text">Encuentra la casa perfecta, que se acople a tus necesidades y a tu presupuesto, con nuestro buscador podras filtrar por: <br>precio<br>localizacion<br>tamano </p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Encuentra mas informacion!</a>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <img class="card-img-top" src="images/depa.jpg" alt="">
          <div class="card-body">
            <h4 class="card-title">Departamentos</h4>
            <p class="card-text">Encuentra el departamento ideal para ti, centricos ideales para cuando vienes a disfrutar de unas buenas vacaciones</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Encuentra mas informacion!</a>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <img class="card-img-top" src="images/locales.jpg" alt="">
          <div class="card-body">
            <h4 class="card-title">Locales Comerciales</h4>
            <p class="card-text">¿Estas por iniciar tu negocio? Encuentra los mejores locales comeciales de la region.</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Encuentra mas informacion!</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  @endsection
  